// Middleware
const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const auth = require("../auth");
const { validateProduct } = require("../validators");

// Access Controllers
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const orderController = require("../controllers/orderController");
const { order } = require("../controllers/orderController");

// Route for creating an order
router.post("/", auth.verify, async (req, res) => {
  try {
    const decodedToken = auth.decode(req.headers.authorization);
    if (decodedToken.isAdmin) {
      return res.status(401).json({ message: "Unauthorized" });
    }

    // Validate the products
    const products = req.body.products;
    const productValidation = validateProduct(products);
    if (productValidation !== true) {
      return res.status(400).json({ message: productValidation.error });
    }

    // Retrieve the prices for each product from the database
    const productsWithPrice = await Promise.all(
      products.map(async (product) => {
        const dbProduct = await Product.findById(product.productId);
        const price = dbProduct.price;
        return {
          productId: product.productId,
          quantity: product.quantity,
          price: price,
          name: dbProduct.name, // Add product name to the returned object
        };
      })
    );

    // Create the order using the order controller
    const savedOrder = await order(decodedToken.id, productsWithPrice);

    // Calculate the total amount based on the products and their prices
    const totalAmount = productsWithPrice.reduce(
      (total, product) => total + product.price * product.quantity,
      0
    );

    // Update the total amount of the order in the database
    await Order.findByIdAndUpdate(savedOrder._id, {
      $set: {
        totalAmount: totalAmount,
      },
    });

    // Log the order details
    console.log(`User Id: ${decodedToken.id}`);
    console.log(
      `Order: ${productsWithPrice.map((product) => product.name).join(", ")}`
    );
    // Join product names with a comma
    console.log(`Total Amount: ${totalAmount}`);
    console.log(`Status: ${savedOrder.status}`);
    console.log(`Purchased on: ${savedOrder.purchasedOn}`);

    res.status(201).json({
      message: `Order for ${productsWithPrice
        .map((product) => product.name)
        .join(", ")} created successfully`,
    }); // Add product names to the success message
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Export the router as a middleware function
module.exports = router;
