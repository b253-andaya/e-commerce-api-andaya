const bcrypt = require("bcrypt");
const mongoose = require("mongoose");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

// Controller for creating an order
module.exports.order = async (userId, products) => {
  try {
    // Retrieve the prices for each product from the database
    const productsWithPrice = await Promise.all(
      products.map(async (product) => {
        if (!product.productId || !product.price) {
          throw new Error("Product ID or price missing");
        }
        const dbProduct = await Product.findById(product.productId);
        const price = dbProduct.price;
        if (!price) {
          throw new Error(`Product ${product.productId} has no price`);
        }
        return {
          productId: product.productId,
          quantity: product.quantity,
          price: price,
        };
      })
    );

    // Calculate the total amount based on the products and their prices
    const totalAmount = productsWithPrice.reduce(
      (total, product) => total + product.price * product.quantity,
      0
    );

    // Create a new order object
    const order = new Order({
      userId,
      products: productsWithPrice,
      totalAmount,
    });

    // Save the order to the database
    const savedOrder = await order.save();

    // Return the saved order
    return savedOrder;
  } catch (err) {
    throw new Error(err.message);
  }
};

