// Middlewares
const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

// Register New User
module.exports.registerUser = async (req, res) => {
  try {
    // Check if email already exists
    const existingUser = await User.findOne({ email: req.body.email });
    if (existingUser) {
      return res.status(400).send({ error: "Email already exists" });
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(req.body.password, 10);

    // Create new user with hashed password
    const newUser = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: hashedPassword,
    });

    // Validate user input
    const errors = newUser.validateSync();
    if (errors) {
      const errorResponse = {
        errors: {},
      };
      for (let key in errors.errors) {
        errorResponse.errors[key] = errors.errors[key].message;
      }
      return res.status(400).send(errorResponse);
    }

    // Save user to database
    await newUser.save();
    return res.status(201).send({ message: "User is successfully registered" });
  } catch (err) {
    console.error(err);
    return res.status(500).send({ error: "User registration is unsuccessful" });
  }
};

// Login a User
// This function logs in a user by checking if the email and password match a user in the database
module.exports.loginUser = async (reqBody) => {
  try {
    // Find user with matching email
    const user = await User.findOne({ email: reqBody.email });
    if (!user) {
      // If user is not found, throw an error
      throw new Error("Invalid email or password");
    }

    const isUsernameCorrect = reqBody.email === user.email;
    const isPasswordCorrect = await bcrypt.compare(
      reqBody.password,
      user.password
    );

    if (!isUsernameCorrect && !isPasswordCorrect) {
      throw new Error("Username and password are incorrect");
    } else if (!isUsernameCorrect) {
      throw new Error("Username is incorrect");
    } else if (!isPasswordCorrect) {
      throw new Error("Password is incorrect");
    }

    // If email and password match, create an access token and return it
    const accessToken = auth.createAccessToken(user);
    if (!accessToken) {
      // If access token creation fails, throw an error
      throw new Error("Failed to create access token");
    }
    return { access: accessToken };
  } catch (err) {
    // If any other errors occur, log the error and throw a generic error message
    console.error(err);
    throw err;
  }
};

// Controller for getting a user's data
module.exports.getProfile = (data) => {
  return User.findById(data.userId).then((result) => {
    result.password = "";
    return result;
  });
};

// Controller for getting all orders
module.exports.getAllOrders = () => {
  return Order.find({})
    .then((result) => result)
    .catch((err) => {
      console.error(err);
      throw new Error(`Error adding orders.`);
    });
};

// Controller for getting a user's order
module.exports.getOrdersForUser = async (userId) => {
  try {
    const orders = await Order.find({ userId });
    return orders;
  } catch (err) {
    throw new Error("Error getting orders for user");
  }
};

// Controller for changing the user to an admin
module.exports.makeAdmin = async (userId) => {
  try {
    // Find the user in the database
    const user = await User.findById(userId);
    if (!user) {
      throw new Error(`User with id ${userId} not found`);
    }

    // If the user is not an admin, make them an admin
    if (!user.isAdmin) {
      user.isAdmin = true;
      await user.save();
    }

    // Omit the password field from the user object
    const { password, ...userDetails } = user.toObject();

    return userDetails;
  } catch (err) {
    throw new Error(`Error making user an admin: ${err.message}`);
  }
};
