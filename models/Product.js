// Connection to MongoDB database
const mongoose = require("mongoose");

// Schema/Data Model Structure
const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Name of the product is required"],
  },
  description: {
    type: String,
    required: [true, "Description of the product is required"],
  },
  price: {
    type: Number,
    required: [true, "Price of the product is required"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
});

// Exports the model from the file as a module
module.exports = mongoose.model("Product", productSchema);
