const express = require("express");
const mongoose = require("mongoose");
// Validate product ID and quantity
module.exports.validateProduct = (products) => {
  const errors = [];

  products.forEach((product) => {
    if (
      !product.productId ||
      !mongoose.Types.ObjectId.isValid(product.productId)
    ) {
      errors.push("Please input a valid product ID");
    }

    if (!product.quantity || product.quantity <= 0) {
      errors.push("Please input a quantity greater than 0");
    }
  });

  if (errors.length > 0) {
    return { error: errors.join(" and ") };
  }

  return true;
};
