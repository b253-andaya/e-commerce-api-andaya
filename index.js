// Required Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Access Routes
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");

// Methods
const app = express();
const port = process.env.PORT || 3000;
const db = mongoose.connection;

// Connect to MongoDB database
mongoose.connect(
  "mongodb+srv://admin:admin123@b253-andaya.ugqjfk2.mongodb.net/E-commerceApp?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
db.once("open", () => console.log("Now connected to MongoDB Atlas"));

// Middleware Functions
// Parses any incoming JSON data and adds it to the req.body object
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Mount middleware functions to specific URL paths
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);

// Listen for incoming HTTP requests on port 3000
if (require.main === module) {
  app.listen(port, () => {
    console.log(`API is now online on port ${port}`);
  });
}

module.exports = app;

// Tapos na sa registration tsaka login
// Product Route and Product Controller
