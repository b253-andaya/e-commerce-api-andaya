const jwt = require("jsonwebtoken");
const secret = "E-commerceApp";

// Token creation
module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  try {
    const token = jwt.sign(data, secret, {});
    return token;
  } catch (err) {
    console.error(err);
    return null;
  }
};

// Token Verification
module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;

  if (typeof token !== "undefined") {
    console.log(token);
    token = token.slice(7, token.length);

    // Validate the token
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        console.error(err);
        return res.status(401).send({ error: "Invalid authorization token" });
      } else {
        next();
      }
    });

    // Token does not exist
  } else {
    return res.status(401).send({ error: "No authorization token provided" });
  }
};

// Token decryption
module.exports.decode = (token) => {
  try {
    if (typeof token !== "undefined") {
      token = token.slice(7, token.length);

      const decodedToken = jwt.verify(token, secret);
      const decodedPayload = jwt.decode(token, { complete: true }).payload;

      return decodedPayload;
    } else {
      return null;
    }
  } catch (err) {
    console.error("Error decoding token:", err);
    return { error: "Invalid or malformed token" };
  }
};
